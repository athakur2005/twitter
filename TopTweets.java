package com.javatechie.spring.bookmyshow.api;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import com.fasterxml.jackson.databind.ObjectMapper;

public class TopTweets {

	private static final String API_KEY = "X2bkRqNcuNGBFmeqCmdu61xBq";
	private static final String API_SECRET = "TDH4Y6afjneyuZ68OzUXTmak0S8wkhbmxGn2w5a0zHRoNlXdPm";

	public static void main(String args[]) throws IOException, URISyntaxException {
		// query parameter
		String response = search("(iPhone");
		System.out.println(response);
	}

	/**
	 * Return the tweets based on the query parameter
	 * 
	 * @param searchString
	 * @return
	 * @throws IOException
	 * @throws URISyntaxException
	 */
	private static String search(String searchString) throws IOException, URISyntaxException {
		String searchResponse = null;

		HttpClient httpClient = HttpClients.custom()
				.setDefaultRequestConfig(RequestConfig.custom().setCookieSpec(CookieSpecs.STANDARD).build()).build();

		URIBuilder uriBuilder = new URIBuilder("https://api.twitter.com/1.1/search/tweets.json");

		ArrayList<NameValuePair> queryParameters;
		queryParameters = new ArrayList<>();
		queryParameters.add(new BasicNameValuePair("q", searchString));
		queryParameters.add(new BasicNameValuePair("count", "10"));
		uriBuilder.addParameters(queryParameters);

		HttpGet httpGet = new HttpGet(uriBuilder.build());
		httpGet.setHeader("Authorization", String.format("Bearer %s", getAccessToken()));
		httpGet.setHeader("Content-Type", "application/json");

		HttpResponse response = httpClient.execute(httpGet);
		HttpEntity entity = response.getEntity();
		if (null != entity) {
			searchResponse = EntityUtils.toString(entity, "UTF-8");
		}
		return searchResponse;
	}

	/**
	 * Code to get the access token based on API key and secret
	 * 
	 * @return
	 * @throws IOException
	 * @throws URISyntaxException
	 */
	private static String getAccessToken() throws IOException, URISyntaxException {
		String accessToken = null;

		HttpClient httpClient = HttpClients.custom()
				.setDefaultRequestConfig(RequestConfig.custom().setCookieSpec(CookieSpecs.STANDARD).build()).build();

		URIBuilder uriBuilder = new URIBuilder("https://api.twitter.com/oauth2/token");
		ArrayList<NameValuePair> postParameters;
		postParameters = new ArrayList<>();
		postParameters.add(new BasicNameValuePair("grant_type", "client_credentials"));
		uriBuilder.addParameters(postParameters);

		HttpPost httpPost = new HttpPost(uriBuilder.build());
		httpPost.setHeader("Authorization", String.format("Basic %s", getBase64EncodedString()));
		httpPost.setHeader("Content-Type", "application/json");

		HttpResponse response = httpClient.execute(httpPost);
		HttpEntity entity = response.getEntity();

		if (null != entity) {
			try (InputStream inputStream = entity.getContent()) {
				ObjectMapper mapper = new ObjectMapper();
				@SuppressWarnings("unchecked")
				Map<String, Object> jsonMap = mapper.readValue(inputStream, Map.class);
				accessToken = jsonMap.get("access_token").toString();
			}
		}
		return accessToken;
	}

	/**
	 * Return the API key ad secret in base64 encoded form
	 * 
	 * @return
	 */
	private static String getBase64EncodedString() {
		String s = String.format("%s:%s", API_KEY, API_SECRET);
		return Base64.getEncoder().encodeToString(s.getBytes(StandardCharsets.UTF_8));
	}
}